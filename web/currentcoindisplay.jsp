<%-- 
    Document   : currentcoindisplay
    Created on : Feb 20, 2018, 9:24:31 PM
    Author     : treypero
--%>

<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.DecimalFormat"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><% out.print(request.getParameter("name") + " information"); %></title>
        <link rel="stylesheet"  href="CSS/styles.css"/>
    </head>
    <body>
        <%@include file="header.html" %>
        <%@include file="navbar.jsp" %>

        <div class="wrapper">
            <div class="content">
                <% 
                    String pagepath = "currentcoindisplay.jsp?coin_index=" + request.getParameter("coin_index") + "&name=" + request.getParameter("name");
                    
                    JSONObject name = new JSONObject();
                    
                    JSONArray favoritesList = (JSONArray)session.getAttribute("favoritesList");
                    JSONArray investmentsList = (JSONArray)session.getAttribute("investmentsList");
                    JSONObject currentCoin = (JSONObject)session.getAttribute("currentCoinData");
                    
                    name.put("name", currentCoin.get("symbol"));

                    DecimalFormat withCommas = new DecimalFormat("#,###");
                    DecimalFormat twoDecimalPlaces = new DecimalFormat("#,##0.00");
                    
                    String marketcap = withCommas.format(Double.parseDouble(currentCoin.get("market_cap_usd").toString()));
                    
                    String price = "";
                    
                    if(Double.parseDouble(currentCoin.get("price_usd").toString()) > 0.01)
                    {
                        price = twoDecimalPlaces.format(Double.parseDouble(currentCoin.get("price_usd")
                                                        .toString()));
                    }
                    else
                    {
                        price = currentCoin.get("price_usd").toString();
                    }
                    
                    String volume = withCommas.format(Double.parseDouble(currentCoin.get("24h_volume_usd").toString()));
                    String supply = withCommas.format(Double.parseDouble(currentCoin.get("available_supply").toString()));
                    
                    String header = ("<h2 style='text-align: left; margin-left: 30px;'>" + currentCoin.get("name") + " (" + currentCoin.get("symbol") + ')');
                    
                    if(favoritesList != null && favoritesList.contains(name))
                    {
                        header += ("<form style= 'display: inline-block; margin-left: 200px' action='FavoritesServlet' method='POST'>");
                        header += ("<input type='hidden' name='redirect' value='" + pagepath +"'/>");
                        header += ("<input type='hidden' name='name' value='" 
                                + currentCoin.get("symbol") + "'" + "/>");
                        header += ("<input type='hidden' name='delete' value='DELETE'/>");
                        header += ("<input class='unfavorite_button' type='submit' value='Unfavorite'/></form>");
                        header += ("</form>");                       
                    }
                    else    // Otherwise give them the option to make it a favorite
                    {
                        header += ("<form style= 'display: inline-block; margin-left: 200px' action='FavoritesServlet' method='POST'>");
                        header += ("<input type='hidden' name='redirect' value='" + pagepath +"'/>");
                        header += ("<input type='hidden' name='name' value='" 
                                + currentCoin.get("symbol") + "'/>");
                        header += ("<input class='favorite_button' type='submit' value='Favorite'/>");
                        header += ("</form>");
                    }
                    
                    boolean investment = false;
                            
                    for(int j = 0; j < investmentsList.size(); j++)
                    {
                        JSONObject currentObj = (JSONObject)investmentsList.get(j);

                        if(currentObj.get("name").equals(currentCoin.get("symbol")))
                        {
                            investment = true;
                            break;
                        }
                    }

                    if(investmentsList != null && investment)
                    {
                        header += ("<form style= 'display: inline-block; margin-left: 200px' action='InvestmentsServlet' method='POST'>");
                        header += ("<input type='hidden' name='redirect' value='" + pagepath +"'/>");

                        header += ("<input type='hidden' name='name' value='" 
                                + currentCoin.get("symbol") + "'/>");
                        header += ("<input type='hidden' name='submit' value='Delete Investment'/>");
                        header += ("<input class='uninvest_button' type='submit' value='Remove Investment'/>");
                        header += ("</form>" + "</h2>");
                    }
                    else
                    {
                        header += ("<form style= 'display: inline-block; margin-left: 200px' action='InvestmentsServlet' method='POST'>");
                        header += ("<input type='hidden' name='redirect' value='" + pagepath +"'/>");
                        header += ("<input type='hidden' name='name' value='" 
                                + currentCoin.get("symbol") + "'/>");
                        header += ("<input class='invest_button' type='submit' value='Add to Investments'/>");
                        header += ("</form>" + "</h2>");
                    }
                    
                    out.print(header);
                    
                    out.print("<div id='currentcoininfo' style='margin-left: 70px;'>");
                    out.print("<h4> Price: $" + price + "</h3>");
                    out.print("<h4> Market Cap: $" + marketcap + "</h3>");
                    out.print("<h4> Volume: $" + volume + "</h3>");
                    out.print("<h4> Supply: " + supply + "</h3>");
                %>
                    
                <table style='margin-left: 0px;'>
                <thead>
                <tr>
                    <th>12 hour Change </th>
                    <th>24 hour Change </th>
                    <th>3 day Change </th>
                    <th>7 day Change </th>
                    <th>1 month Change </th>
                </tr>
                </thead>
                <tbody>
                <%                     
                    out.print("<tr>");
                    if(currentCoin.get("percent_change_12h") != null)
                    {
                        out.print("<td>" + twoDecimalPlaces.format(Double.parseDouble(currentCoin
                                        .get("percent_change_12h").toString())) + "%</td>");
                    }
                    else
                    {
                        out.print("<td>N/A</td>");
                    }
                    
                    if(currentCoin.get("percent_change_24h") != null)
                    {
                        out.print("<td>" + twoDecimalPlaces.format(Double.parseDouble(currentCoin
                                        .get("percent_change_24h").toString())) + "%</td>");
                    }
                    else
                    {
                        out.print("<td>N/A</td>");
                    }
                    
                    if(currentCoin.get("percent_change_3d") != null)
                    {
                        out.print("<td>" + twoDecimalPlaces.format(Double.parseDouble(currentCoin
                                        .get("percent_change_3d").toString())) + "%</td>");
                    }
                    else
                    {
                        out.print("<td>N/A</td>");
                    }
                    
                    if(currentCoin.get("percent_change_7d") != null)
                    {
                        out.print("<td>" + twoDecimalPlaces.format(Double.parseDouble(currentCoin
                                        .get("percent_change_7d").toString())) + "%</td>");
                    }
                    else
                    {
                        out.print("<td>N/A</td>");
                    }
                    
                    if(currentCoin.get("percent_change_1m") != null)
                    {
                        out.print("<td>" + twoDecimalPlaces.format(Double.parseDouble(currentCoin
                                        .get("percent_change_1m").toString())) + "%</td>");    
                    }
                    else
                    {
                        out.print("<td>N/A</td>");
                    }
                    
                    out.print("</div>");
                %>
                </table>
                
                <%
                    if (currentCoin.get("symbol").equals("BTC"))
                    {
                        
                        out.print("<div style=\"margin-top:20px\">"
                                 +"<div id=\"rssincl-box-container-1172210\"></div>"
                                 +"<script type=\"text/javascript\">"
                                 +"(function() {"
                                 +"var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;"
                                 +"s.src = 'http://output50.rssinclude.com/output?type=asyncjs&id=1172210&hash=8e92e2b3da987f9cb639916e75f68020';"
                                 +"document.getElementsByTagName('head')[0].appendChild(s);"
                                 +"})();</script></div>");
                    }else if(currentCoin.get("symbol").equals("MON")){
                    
                        out.print("<div style=\"margin-top:20px\">"
                                 +"<div id=\"rssincl-box-container-1172211\"></div>"
                                 +"<script type=\"text/javascript\">"
                                 +"(function() {"
                                 +"var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;"
                                 +"s.src = 'http://output52.rssinclude.com/output?type=asyncjs&id=1172211&hash=36a6d85980f0c378ffaa88ccbb0756a2';"
                                 +"document.getElementsByTagName('head')[0].appendChild(s);"
                                 +"})();</script></div>");
                    }else if(currentCoin.get("symbol").equals("ETH")){
                    
                        out.print("<div style=\"margin-top:20px\">"
                                  +"<div id=\"rssincl-box-container-1172212\"></div>"
                                  +"<script type=\"text/javascript\">"
                                  +"(function() {"
                                  +"var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;"
                                  +"s.src = 'http://output59.rssinclude.com/output?type=asyncjs&id=1172212&hash=9c68e66b7a233fcaeff168cd0126d901';"
                                  +"document.getElementsByTagName('head')[0].appendChild(s);"
                                  +"})();</script></div>");
                    }else if(currentCoin.get("symbol").equals("LTC")){
                    
                        out.print("<div style=\"margin-top:20px\">"
                                 +"<div id=\"rssincl-box-container-1172213\"></div>"
                                 +"<script type=\"text/javascript\">"
                                 +"(function() {"
                                 +"var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;"
                                 +"s.src = 'http://output80.rssinclude.com/output?type=asyncjs&id=1172213&hash=8f57a6e3e0bc0f56ddf43ad46121565f';"
                                 +"document.getElementsByTagName('head')[0].appendChild(s);"
                                 +"})();</script></div>");      
                    }else{
                    
                        out.print("<div style=\"margin-top:20px\">"
                                 +"<div id=\"rssincl-box-container-1171873\"></div>"
                                 +"<script type=\"text/javascript\">"
                                 +"(function() {"
                                 +"var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;"
                                 +"s.src = 'http://output66.rssinclude.com/output?type=asyncjs&id=1171873&hash=7043c6521d01a0d1266f43652e1e832c';"
                                 +"document.getElementsByTagName('head')[0].appendChild(s);"
                                 +"})();</script></div>");
                    }// end if
                %>
            </div>
        </div>
    </body>
</html>
