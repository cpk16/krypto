<%-- 
    Document   : favoritesdisplay
    Created on : Feb 17, 2018, 1:20:09 AM
    Author     : treypero
--%>
<%@page import="org.json.simple.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.DecimalFormat"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Favorite Cryptocurrencies</title>
        <link rel="stylesheet" href="CSS/styles.css"/>
    </head>
    <body>
        
        <%@include file="header.html" %>
        <%@include file="navbar.jsp" %>
        
        <div class="wrapper">
            

            <div class="content">
                <h2>My Favorite Cryptocurrencies</h2>
                <table id="favoritesTable">
                    <!-- Table headers -->
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Market Cap </th>
                        <th>Price </th>
                        <th>Trade Volume </th>
                        <th>Supply </th>
                        <th>24 hour Change </th>
                        <th>7 day Change </th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                        <%
                            JSONArray favoritesList = (JSONArray)session.getAttribute("favoritesList");

                            for(int i = 0; i < favoritesList.size(); i++)
                            {
                                JSONObject userFavorite = (JSONObject)favoritesList.get(i);
                                DecimalFormat withCommas = new DecimalFormat("#,###");
                                DecimalFormat twoDecimalPlaces = new DecimalFormat("#,###.00");
                                
                                out.print("<td><form action='CoinDataServlet' method='GET'>"
                                        + "<input class='namebutton' type='submit' name='name' value='"
                                        + userFavorite.get("name") 
                                        + "'/></form></td>");

                                out.print("<td>$" + withCommas.format(Double.parseDouble(
                                                                      userFavorite.get("market_cap_usd")
                                                                                     .toString())) + "</td>");
                                out.print("<td>$" + twoDecimalPlaces.format(Double.parseDouble(
                                                                        userFavorite.get("price_usd")
                                                                        .toString())) + "</td>");
                                out.print("<td>$" + withCommas.format(Double.parseDouble(
                                                                      userFavorite.get("24h_volume_usd")
                                                                      .toString())) + "</td>");
                                out.print("<td>" + withCommas.format(Double.parseDouble(
                                                                     userFavorite.get("available_supply")
                                                                             .toString())) + "</td>");
                                out.print("<td>" + userFavorite.get("percent_change_24h") + "%</td>");
                                out.print("<td>" + userFavorite.get("percent_change_7d") + "%</td>");
                                out.print("<td><form action='FavoritesServlet' method='POST'>");
                                out.print("<input type='hidden' name='name' value='" 
                                        + userFavorite.get("name") + "'/>");
                                out.print("<input type='hidden' name='delete' value='DELETE'/>");
                                out.print("<input type='hidden' name='redirect' value='favoritesdisplay.jsp'/>");
                                out.print("<input class='unfavorite_button' type='submit' value='Unfavorite'/></form>");
                                out.print("</tr>");
                            }
                        %>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
