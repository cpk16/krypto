<%-- 
    Document   : test
    Created on : Feb 16, 2018, 4:14:42 PM
    Author     : treypero
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1><% session.getAttribute("greeting"); %></h1>
        <form action="TestServlet" method="GET">
            <input type="submit" value="Submit"/>
        </form>
    </body>
</html>
