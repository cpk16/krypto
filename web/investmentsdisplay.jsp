<%-- 
    Document   : investmentsdisplay
    Created on : Feb 17, 2018, 1:20:14 AM
    Author     : treypero
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Cryptocurrency Investments</title>
        <link rel="stylesheet" href="CSS/styles.css"/>
    </head>
    <body>
        
        <%@include file="header.html" %>
        <%@include file="navbar.jsp" %>
        
        <div class="wrapper">
            <div class="content">
                <h2>My Investments</h2>
                <table id="investmentTable">
                    <!-- Table headers -->
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price Paid</th>
                        <th>Current Coin Price</th>
                        <th>Net Value</th>
                        <th>Net %</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                        <%
                            JSONArray investmentsList = (JSONArray)session.getAttribute("investmentsList");

                            for(int i = 0; i < investmentsList.size(); i++)
                            {
                                JSONObject userInvestment = (JSONObject)investmentsList.get(i);
                                DecimalFormat formatter = new DecimalFormat("#,##0.00");
                                
                                out.print("<tr>");

                                out.print("<td><form action='CoinDataServlet' method='GET'>"
                                        + "<input class='namebutton' type='submit' name='name' value='"
                                        + userInvestment.get("name") + "'/></form></td>");
                                out.print("<form action='InvestmentsServlet' method='POST'>");
                                out.print("<td><input name='quantity' value='" + userInvestment.get("quantity") + "'/></td>");
                                out.print("<td>$<input name='price_paid' value='" + formatter.format(Double.parseDouble(userInvestment.get("price_paid").toString())) + "'/></td>");
                                out.print("<td>$" + formatter.format((double)userInvestment.get("current_price")) + "</td>");
                                out.print("<td>$" + formatter.format((double)userInvestment.get("net_value")) + "</td>");
                                out.print("<td>" + formatter.format((double)userInvestment.get("net_percent")) + "%</td>");
                                out.print("<td>");
                                out.print("<input class='updateinvestment' type='submit' name='submit' value='Update Investment'/>");
                                out.print("<input type='hidden' name='name' value='" + userInvestment.get("name") + "'/>");
                                out.print("</td>");
                                out.print("<td><form action='InvestmentsServlet' method='POST'>");
                                out.print("<input type='hidden' name='name' value='" + userInvestment.get("name") + "'/>");
                                out.print("<input class='uninvest_button' type='submit' name='submit' value='Delete Investment'/>");
                                out.print("</form></td>");
                                out.print("</tr>");
                            }
                        %>
                    </tbody>
                </table>
            </div>
        </div>
            
    </body>
</html>