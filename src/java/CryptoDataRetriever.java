
import java.net.*;
import java.time.Instant;
import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import jdk.nashorn.internal.runtime.ParserException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author treypero
 */
public class CryptoDataRetriever
{
    // Gets the coin data from whatever api(s) you decide on
    public JSONArray getCoinData() throws ParseException, IOException       
    {
        JSONArray coinDataList = new JSONArray();
        
        URL coinData = new URL ("https://api.coinmarketcap.com/v1/ticker/?limit=500");
        
        HttpURLConnection connection = (HttpURLConnection) coinData.openConnection();
        int responsecode = connection.getResponseCode();
        System.out.println("Response Code: " + responsecode );
        
        coinDataList = (JSONArray)new JSONParser().parse(new InputStreamReader(connection.getInputStream()));
        
        return coinDataList;
    }
    
    public JSONObject getCoinPriceChangesOverTime(String coinsymbol) throws Exception
    {
        final long seconds12hours = 43200;
        final long seconds3days = 259200;
        final long seconds1month = 2629746;
        long currenttime = Instant.now().getEpochSecond();
        URL urlcoinhist1 = new URL  ("https://min-api.cryptocompare.com/data/pricehistorical?fsym=" + coinsymbol +  "&tsyms=USD&ts=" + (currenttime - seconds12hours));
        URL urlcoinhist2 = new URL  ("https://min-api.cryptocompare.com/data/pricehistorical?fsym=" + coinsymbol +  "&tsyms=USD&ts=" + (currenttime - seconds3days));
        URL urlcoinhist3 = new URL  ("https://min-api.cryptocompare.com/data/pricehistorical?fsym=" + coinsymbol +  "&tsyms=USD&ts=" + (currenttime - seconds1month));
        JSONParser parser = new JSONParser();
        JSONObject coininfo = new JSONObject();
        
        
        //Request Coin Value 12 hours ago
        HttpURLConnection connection = (HttpURLConnection) urlcoinhist1.openConnection();
        connection.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();
        JSONObject temp = (JSONObject)parser.parse(response.toString());
        temp = temp = (JSONObject)temp.get(coinsymbol);
        coininfo.put("12h_change", temp.get("USD"));
        
        
        //Request Coin Value 3 Days Ago
        connection = (HttpURLConnection) urlcoinhist2.openConnection();
        connection.setRequestMethod("GET");
        in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        response = new StringBuffer();
        while ((inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();
        temp = (JSONObject)parser.parse(response.toString());
        temp = (JSONObject)temp.get(coinsymbol);
        coininfo.put("3d_change", temp.get("USD"));
        
        //Request Coin Value 1 Month Ago
        connection = (HttpURLConnection) urlcoinhist3.openConnection();
        connection.setRequestMethod("GET");
        in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        response = new StringBuffer();
        while ((inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();
        temp = (JSONObject)parser.parse(response.toString());
        temp = temp = (JSONObject)temp.get(coinsymbol);
        coininfo.put("1m_change", temp.get("USD"));
        
        
        return coininfo;
    }
    
    // To sort the data by a given key
    public JSONArray sort(String sortKey) throws Exception
    {
        JSONArray coindata = this.getCoinData();
        
        Collections.sort(coindata, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject a, JSONObject b) {

                if (sortKey.equals("name"))
                {
                    String valA = null;
                    String valB = null;

                    try {
                        valA = a.get(sortKey).toString();
                        valB = b.get(sortKey).toString();
                    }
                    catch (ParserException e) {
                        System.out.println("JSONException" + e.getMessage());
                    }

                    return valA.toLowerCase().compareTo(valB.toLowerCase());
                }
                else
                {
                    Double valA = null;
                    Double valB = null;

                    try 
                    {
                        if(a.get(sortKey) != null)
                        {
                            valA = Double.parseDouble(a.get(sortKey).toString());
                        }
                        else
                        {
                            valA = -1000000.0;
                        }
                        if(b.get(sortKey) != null)
                        {
                            valB = Double.parseDouble(b.get(sortKey).toString());
                        }
                        else
                        {
                            valB = -1000000.0;
                        }
                    }
                    catch (ParserException e) {
                        System.out.println("JSONException" + e.getMessage());
                    }

                    return valB.compareTo(valA);
                }
            }
        });
        
        return coindata;
    }

    
    public static void main(String[] args) throws Exception{
        CryptoDataRetriever test = new CryptoDataRetriever();
        //test.getCoinData();
        test.getCoinPriceChangesOverTime("BTC");
    }
}
