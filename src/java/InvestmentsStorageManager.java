
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author treypero
 */
public class InvestmentsStorageManager extends JSONFileManager
{
    public InvestmentsStorageManager() throws IOException, ParseException
    {
        super("investments.json");
    }
    
    // Returns true/false based on whether the add worked successfully
    public boolean addInvestment(JSONObject newInvestment)
    {
        return super.addObject(newInvestment);
    }
    
    // Returns true/false based on whether the remove worked successfully
    public boolean removeInvestment(JSONObject prevInvestment)
    {
        return super.removeObject(prevInvestment);
    }
    
    public JSONArray getInvestmentsList()
    {
        try
        {
            return super.getJSON();
        }
        catch(IOException | ParseException ex)
        {
            return new JSONArray();
        }
    }
}
