/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author treypero
 */
@WebServlet(urlPatterns =
{
    "/FavoritesServlet"
})
public class FavoritesServlet extends HttpServlet
{

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            JSONArray favoritesList = new FavoritesStorageManager().getFavoritesList();
            JSONArray coinDataList = (JSONArray)request.getSession().getAttribute("coinDataList");
            
            for(int i = 0; i < favoritesList.size(); i++)
            {
                JSONObject currentFavorite = (JSONObject)favoritesList.get(i);
                
                for(int j = 0; j < coinDataList.size(); j++)
                {
                    JSONObject currentCoinData = (JSONObject)coinDataList.get(j);
                    
                    if(currentCoinData.get("symbol").equals(currentFavorite.get("name")))
                    {
                        currentFavorite.put("market_cap_usd", currentCoinData.get("market_cap_usd"));
                        currentFavorite.put("price_usd", currentCoinData.get("price_usd"));
                        currentFavorite.put("24h_volume_usd", currentCoinData.get("24h_volume_usd"));
                        currentFavorite.put("available_supply", currentCoinData.get("available_supply"));
                        currentFavorite.put("percent_change_24h", currentCoinData.get("percent_change_24h"));
                        currentFavorite.put("percent_change_7d", currentCoinData.get("percent_change_7d"));
                        break;
                    }
                }
            }

            request.getSession().setAttribute("favoritesList", favoritesList);
            
            if(request.getParameter("redirect") != null)
            {
                request.getRequestDispatcher(request.getParameter("redirect")).forward(request, response);
            }
            else
            {
                request.getRequestDispatcher("favoritesdisplay.jsp").forward(request, response);
            }
        }
        catch(ParseException ex)
        {
            System.err.println(ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        if(request.getParameter("delete") != null)
        {
            try
            {
                JSONObject obj = new JSONObject();
                obj.put("name", request.getParameter("name"));
                FavoritesStorageManager manager = new FavoritesStorageManager();
                manager.removeFavorite(obj);

                request.getSession().setAttribute("favoritesList", manager.getFavoritesList());
            }
            catch(ParseException ex)
            {
                System.err.println(ex);
            }
        }
        else
        {
            try
            {
                JSONObject obj = new JSONObject();
                obj.put("name", request.getParameter("name"));
                FavoritesStorageManager manager = new FavoritesStorageManager();
                manager.addFavorite(obj);
                request.getSession().setAttribute("favoritesList", manager.getFavoritesList());
            }
            catch(ParseException ex)
            {
                System.err.println(ex);
            }
        }
        
        if(request.getParameter("redirect").contains("cryptodatadisplay.jsp"))
        {
            new CryptoDataServlet().doGet(request, response);
        }
        else if(request.getParameter("redirect").contains("currentcoindisplay.jsp"))
        {
            new CoinDataServlet().doGet(request, response);
        }
        else
        {
            doGet(request, response);
        }
    }
    
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
