/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.simple.*;
import org.json.simple.parser.ParseException;

/**
 *
 * @author treypero
 */
@WebServlet(urlPatterns =
{
    "/CoinDataServlet"
})
public class CoinDataServlet extends HttpServlet
{

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        JSONArray coinDataList = new JSONArray();
        JSONObject currentCoinData = new JSONObject();
        JSONArray favoritesList = new JSONArray();
        JSONArray investmentsList = new JSONArray();
        
        try
        {
            coinDataList = new CryptoDataRetriever().getCoinData();
            favoritesList = new FavoritesStorageManager().getFavoritesList();
            investmentsList = new InvestmentsStorageManager().getInvestmentsList();
            
            // Find the current associated coin data
            for(int i = 0; i < coinDataList.size(); i++)
            {
                currentCoinData = (JSONObject)coinDataList.get(i);
                
                if(currentCoinData.get("symbol").equals(request.getParameter("name")))
                {
                    break;
                }
            }
            
            JSONObject changes = new CryptoDataRetriever()
                    .getCoinPriceChangesOverTime(currentCoinData.get("symbol").toString());
            
            if((Double.parseDouble(changes.get("12h_change").toString())) > 0.0)
            {
                currentCoinData.put("percent_change_12h", ((Double.parseDouble(currentCoinData.get("price_usd").toString()) - 
                        Double.parseDouble(changes.get("12h_change").toString()) ) / 
                        Double.parseDouble(changes.get("12h_change").toString()) * (double)100));
            }
            else
            {
                currentCoinData.put("percent_change_12h", null);
            }
            
            if((Double.parseDouble(changes.get("3d_change").toString())) > 0.0)
            {
                currentCoinData.put("percent_change_3d", ((Double.parseDouble(currentCoinData.get("price_usd").toString()) - 
                        Double.parseDouble(changes.get("3d_change").toString()) ) / 
                        Double.parseDouble(changes.get("3d_change").toString())* (double)100));
            }
            else
            {
                currentCoinData.put("percent_change_3d", null);
            }
            
            if((Double.parseDouble(changes.get("1m_change").toString())) > 0.0)
            {
                currentCoinData.put("percent_change_1m", ((Double.parseDouble(currentCoinData.get("price_usd").toString()) - 
                        Double.parseDouble(changes.get("1m_change").toString()) ) / 
                        Double.parseDouble(changes.get("1m_change").toString())* (double)100));
            }
            else
            {
                currentCoinData.put("percent_change_1m", null);
            }
          
            request.getSession().setAttribute("currentCoinData", currentCoinData);
            request.getSession().setAttribute("favoritesList", favoritesList);
            request.getSession().setAttribute("investmentsList", investmentsList);
        }
        catch(Exception ex)
        {
            System.err.println(ex);
        }
        
        request.getRequestDispatcher("currentcoindisplay.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
