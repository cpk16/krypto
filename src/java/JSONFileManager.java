
import java.io.File;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileReader;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author treypero
 */
public class JSONFileManager
{
    private final String FILE_PATH;
    JSONArray jsonArray;
    
    public JSONFileManager(String filePath) throws IOException
    {
        FILE_PATH = filePath;
        jsonArray = new JSONArray();
        
        if(!new File(FILE_PATH).exists())
        {
            new File(FILE_PATH).createNewFile();
        }
        
        try
        {
            jsonArray = getJSON();
        }
        catch(ParseException ex)
        {
            jsonArray = new JSONArray();
        }
    }
    
    //add to file
    public boolean addObject(JSONObject obj)
    {
        boolean flag;

        try
        {
            if(obj != null) jsonArray.add(obj);
            
            FileWriter file = new FileWriter(FILE_PATH);
            file.write(jsonArray.toJSONString());
            
            file.flush();
            file.close();
            flag = true;
        }
        catch(IOException e)
        {
            e.printStackTrace();
            flag = false;
        }
        
        return flag;
    }
    
    //remove from file
    public boolean removeObject(JSONObject obj)
    {
        boolean flag;

        try
        {
            jsonArray.remove(obj);
            System.out.println(jsonArray);
            FileWriter file = new FileWriter(FILE_PATH);
            file.write(jsonArray.toJSONString());
            
            file.flush();
            file.close();
            flag = true;
        }
        catch(IOException e)
        {
            e.printStackTrace();
            flag = false;
        }
        
        return flag;
            
    }
    
    
    //NOT DONE
    //read file to JSONArray, return array
    public JSONArray getJSON() throws IOException, ParseException
    {
        JSONParser parser = new JSONParser();
        
        Object obj = parser.parse(new FileReader(FILE_PATH));
            
        jsonArray = (JSONArray)obj;
        
        
        return jsonArray;
    }
}
