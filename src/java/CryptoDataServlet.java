/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.simple.*;
import org.json.simple.parser.ParseException;

/**
 *
 * @author treypero
 */
@WebServlet(urlPatterns =
{
    "/CryptoDataServlet"
})
public class CryptoDataServlet extends HttpServlet
{
    ArrayList<String> test = new ArrayList<>();
    int i = 0;
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        // TEST DATA FOR UNTIL THE API MANAGER IS FINISHED
        JSONArray coinDataList = new JSONArray();
        JSONArray favoritesList = new JSONArray();
        JSONArray investmentsList = new JSONArray();
        
        // Calculate/add necessary session data
        HttpSession session = request.getSession();
        
        try
        {

            coinDataList = new CryptoDataRetriever().getCoinData();
            favoritesList = new FavoritesStorageManager().getFavoritesList();
            investmentsList = new InvestmentsStorageManager().getInvestmentsList();

            if(request.getParameter("sortKey") != null)
            {
                request.getSession().setAttribute("sortKey", request.getParameter("sortKey"));
                coinDataList = new CryptoDataRetriever().sort(request.getParameter("sortKey"));
            }
            else
            {
                if(request.getSession().getAttribute("sortKey") != null)
                {
                    coinDataList = new CryptoDataRetriever().sort(request.getSession().getAttribute("sortKey").toString());
                }
                else
                {
                    coinDataList = new CryptoDataRetriever().getCoinData();
                }
            }
        }
        catch(Exception e)
        {
            System.err.println(e);
        }

        request.getSession().setAttribute("coinDataList", coinDataList);
        request.getSession().setAttribute("favoritesList", favoritesList);
        request.getSession().setAttribute("investmentsList", investmentsList);
        request.getSession().setAttribute("test", i++);
        
        session.setAttribute("coinDataList", coinDataList);
        int maxNum = coinDataList.size() / 100;
        session.setAttribute("maxNumOfPages", maxNum == 0 ? 1 : maxNum);
        Integer currentPageNum = (Integer)session.getAttribute("currentPageNum");
        
        if(currentPageNum == null) currentPageNum = 1;
        
        if(request.getParameter("prev") != null) 
        {
            if(currentPageNum > 1) currentPageNum--;
        }
        else if(request.getParameter("next") != null)
        {
            if(currentPageNum < (int)session.getAttribute("maxNumOfPages")) currentPageNum++;
        }
        
        session.setAttribute("currentPageNum", currentPageNum);
        request.getRequestDispatcher("cryptodatadisplay.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
