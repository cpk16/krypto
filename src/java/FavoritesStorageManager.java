
import java.io.IOException;
import org.json.simple.*;
import org.json.simple.parser.ParseException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author treypero
 */
public class FavoritesStorageManager extends JSONFileManager
{
    public FavoritesStorageManager() throws IOException, ParseException
    {
        super(System.getProperty("user.dir") + "//favorites.json");
    }
    
    // Returns true/false based on whether the add worked successfully
    public boolean addFavorite(JSONObject newFavorite)
    {
        return super.addObject(newFavorite);
    }
    
    // Returns true/false based on whether the remove worked successfully
    public boolean removeFavorite(JSONObject noLongerFav)
    {
        return super.removeObject(noLongerFav);
    }
    
    public JSONArray getFavoritesList()
    {
        try
        {
            return super.getJSON();
        }
        catch(IOException | ParseException ex)
        {
            return new JSONArray();
        }
    }
}
