/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 *
 * @author treypero
 */
@WebServlet(urlPatterns =
{
    "/InvestmentsServlet"
})
public class InvestmentsServlet extends HttpServlet
{
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        JSONArray investmentsList = new JSONArray();
        JSONArray coinDataList = new JSONArray(); 

        try
        {
            investmentsList = new InvestmentsStorageManager().getInvestmentsList();
            coinDataList = new CryptoDataRetriever().getCoinData();
        }
        catch(ParseException ex)
        {
            
        }
        
        for(int i = 0; i < investmentsList.size(); i++)
        {
            JSONObject currentInvestment = (JSONObject)investmentsList.get(i);

            for(int j = 0; j < coinDataList.size(); j++)
            {
                JSONObject currentCoinData = (JSONObject)coinDataList.get(j);

                if(currentCoinData.get("symbol").equals(currentInvestment.get("name")))
                {
                    currentInvestment.put("current_price", Double.parseDouble(currentCoinData.get("price_usd").toString()));
                    currentInvestment.put("net_value", Double.parseDouble(currentCoinData.get("price_usd").toString())
                                                     * Integer.parseInt(currentInvestment.get("quantity").toString())
                                                     - Double.parseDouble(currentInvestment.get("price_paid").toString()));
                    currentInvestment.put("net_percent", (double)currentInvestment.get("net_value") / 
                              Double.parseDouble(currentInvestment.get("price_paid").toString()) * 100);
                    break;
                }
            }
        }
        
        request.getSession().setAttribute("investmentsList", investmentsList);
        
        if(request.getParameter("redirect") != null)
        {
            request.getRequestDispatcher(request.getParameter("redirect")).forward(request, response);
        }
        else
        {
            request.getRequestDispatcher("investmentsdisplay.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        JSONObject obj = new JSONObject();
        JSONArray investmentsList = new JSONArray();
        InvestmentsStorageManager manager = null; 
        String submit = request.getParameter("submit") == null ? "" : request.getParameter("submit");
        
        if(submit.equals("Update Investment"))
        {
            try
            {
                manager = new InvestmentsStorageManager();
                investmentsList = manager.getInvestmentsList();
                
                for(int i = 0; i < investmentsList.size(); i++)
                {
                    JSONObject currentInvestment = (JSONObject)investmentsList.get(i);
                
                    if(currentInvestment.get("name").equals(request.getParameter("name")))
                    {
                        System.out.println("QUANTITY: " + request.getParameter("quantity"));
                        currentInvestment.put("quantity", request.getParameter("quantity"));
                        System.out.println("PRICE: " + request.getParameter("price_paid"));
                        currentInvestment.put("price_paid", request.getParameter("price_paid"));
                        
                        manager.addInvestment(null);
                        break;
                    }
                }
                
                request.getSession().setAttribute("investmentsList", manager.getInvestmentsList());
            }
            catch(ParseException ex)
            {
                System.err.println(ex);
            }
        }
        else if(submit.equals("Delete Investment"))
        {
            try
            {
                manager = new InvestmentsStorageManager();
                investmentsList = manager.getInvestmentsList();

                for(int i = 0; i < investmentsList.size(); i++)
                {
                    JSONObject currentInvestment = (JSONObject)investmentsList.get(i);

                    if(currentInvestment.get("name").equals(request.getParameter("name")))
                    {
                        manager.removeInvestment(currentInvestment);
                        break;
                    }
                }
                
                request.getSession().setAttribute("investmentsList", manager.getInvestmentsList());
            }
            catch(ParseException ex)
            {
                System.err.println(ex);
            }
        }
        else
        {
            obj.put("name", request.getParameter("name"));
            obj.put("quantity", 0);
            obj.put("price_paid", 0.0);
        
            try
            {
                new InvestmentsStorageManager().addInvestment(obj);
            }
            catch(ParseException ex)
            {
                System.err.println(ex);
            }
        }
        
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

}
