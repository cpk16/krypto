<%-- 
    Document   : favoritesdisplay
    Created on : Feb 17, 2018, 1:20:09 AM
    Author     : treypero
--%>
<%@page import="org.json.simple.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Favorite Cryptocurrencies</title>
        <link rel="stylesheet" href="CSS/styles.css"/>
    </head>
    <body>
        
        <%@include file="header.html" %>
        <%@include file="navbar.jsp" %>
        
        <div class="wrapper">
            

            <div class="content">
                <h2>My Favorite Cryptocurrencies</h2>
                <table id="favoritesTable">
                    <!-- Table headers -->
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Market Cap </th>
                        <th>Price </th>
                        <th>Trade Volume </th>
                        <th>Supply </th>
                        <th>24 hour Change </th>
                        <th>12 hour Change </th>
                        <th>3 day Change </th>
                        <th>7 day Change </th>
                        <th>1 month Change </th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                        <%
                            JSONArray favoritesList = (JSONArray)session.getAttribute("favoritesList");

                            for(int i = 0; i < favoritesList.size(); i++)
                            {
                                JSONObject userFavorite = (JSONObject)favoritesList.get(i);

                                out.print("<tr>");
                                out.print("<td>" + userFavorite.get("name") + "</td>");
                                out.print("<td>$" + userFavorite.get("market_cap") + "</td>");
                                out.print("<td>$" + userFavorite.get("price") + "</td>");
                                out.print("<td>$" + userFavorite.get("trade_volume") + "</td>");
                                out.print("<td>" + userFavorite.get("supply") + "</td>");
                                out.print("<td>" + userFavorite.get("12_hour_change") + "%</td>");
                                out.print("<td>" + userFavorite.get("24_hour_change") + "%</td>");
                                out.print("<td>" + userFavorite.get("3_day_change") + "%</td>");
                                out.print("<td>" + userFavorite.get("weekly_change") + "%</td>");
                                out.print("<td>" + userFavorite.get("monthly_change") + "%</td>");
                                out.print("<td><input type='button' value='Delete' onclick='deleteRow(this)'></td>");
                                out.print("</tr>");
                            }
                        %>
                    </tbody>
                </table>
            </div>
        </div>
            
            <script>
                function deleteRow(r) {
                var i = r.parentNode.parentNode.rowIndex;
                document.getElementById("favoritesTable").deleteRow(i);
                }
            </script>
    </body>
</html>
