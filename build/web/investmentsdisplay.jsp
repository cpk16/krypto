<%-- 
    Document   : investmentsdisplay
    Created on : Feb 17, 2018, 1:20:14 AM
    Author     : treypero
--%>

<%@page import="org.json.simple.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My Cryptocurrency Investments</title>
        <link rel="stylesheet" href="CSS/styles.css"/>
    </head>
    <body>
        
        <%@include file="header.html" %>
        <%@include file="navbar.jsp" %>
        
        <div class="wrapper">
            <div class="content">
                <h2>My Investments</h2>
                <table id="investmentTable">
                    <!-- Table headers -->
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price Paid</th>
                        <th>Current Coin Price</th>
                        <th>Net Value</th>
                        <th>Net %</th>
                    </tr>
                    </thead>

                    <tbody>
                        <%
                            JSONArray investmentsList = (JSONArray)session.getAttribute("investmentsList");

                            for(int i = 0; i < investmentsList.size(); i++)
                            {
                                JSONObject userInvestment = (JSONObject)investmentsList.get(i);

                                out.print("<tr>");
                                out.print("<td>" + userInvestment.get("name") + "</td>");
                                out.print("<td>" + userInvestment.get("current_quantity") + "</td>");
                                out.print("<td>$" + userInvestment.get("total_price_paid") + "</td>");
                                out.print("<td>$" + userInvestment.get("current_price_per_coin") + "</td>");
                                out.print("<td>" + userInvestment.get("net_value") + "</td>");
                                out.print("<td>" + userInvestment.get("net_gain_or_loss") + "</td>");
                                out.print("</tr>");
                            }
                        %>
                    </tbody>
                </table>
                <div id="newinvestment">
                <h4>New Investment</h4>
                <form action="">
                    <p>Currency Ticker: </p>
                    <input type="text" name="coinname"><br>
                    <p>Quantity: </p>
                    <input type="text" name="coinquantity"><br>
                    <p>Price Paid (USD): </p>
                    <input type="text" name="coinprice"><br>
                    <p id="submitbutton"> <input type="submit" value="Submit"></p>
                </form>
                </div>
            </div>
        </div>
            
    </body>
</html>