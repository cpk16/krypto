
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.json.simple.*"%>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<html>
    <head>
        <title>CryptoTracker</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet"  href="CSS/styles.css"/>
    </head>
    <body>
        <%@include file="header.html" %>
        <%@include file="navbar.jsp" %>
        <div class="wrapper">
            <div class="content">
                <h2>Cryptocurrency Data</h2>
                <table>
                    <!-- Table headers -->
                    <thead>
                    <tr>
                        <th></th>
                        <th>
                            <form action="CryptoDataServlet" method="GET">
                                <input type='hidden' name='sortKey' value='name'/>
                                <input type="submit" value='Name'/>
                            </form>
                        </th>
           
                        <th>
                            <form action="CryptoDataServlet" method="GET">
                                <input type='hidden' name='sortKey' value='market_cap_usd'/>
                                <input type="submit" value='Market Cap'/>
                            </form>
                        </th>
                
                        <th>
                            <form action="CryptoDataServlet" method="GET">
                                <input type='hidden' name='sortKey' value='price_usd'/>
                                <input type="submit" value='Price'/>
                            </form>
                        </th>
               
                        <th>
                            <form action="CryptoDataServlet" method="GET">
                                <input type='hidden' name='sortKey' value='24h_volume_usd'/>
                                <input type="submit" value='Trade Volume'/>
                            </form>
                        </th>
        
                        <th>
                            <form action="CryptoDataServlet" method="GET">
                                <input type='hidden' name='sortKey' value='total_supply'/>
                                <input type="submit" value='Supply'/>
                            </form>
                        </th>
           
                        <th>
                            <form action="CryptoDataServlet" method="GET">
                                <input type='hidden' name='sortKey' value='percent_change_24h'/>
                                <input type="submit" value='24 Hour Change'/>
                            </form>
                        </th>
   
                        <th>
                            <form action="CryptoDataServlet" method="GET">
                                <input type='hidden' name='sortKey' value='percent_change_7d'/>
                                <input type="submit" value='7 Day Change'/>
                            </form>
                        </th>
                        <th></th>
                    </thead>

                    <tbody>

                    <%
                        JSONArray coinDataList = (JSONArray)session.getAttribute("coinDataList");
                        ArrayList<String> favoritesList = (ArrayList)session.getAttribute("favoritesList");
                        int numOfCoinsOnPage = Math.min(100, coinDataList.size());
                        int currentPageNum = (int)session.getAttribute("currentPageNum");
                        int startIndex = currentPageNum == 1 ? 0 : ((currentPageNum - 1) * 100);

                        // Page 1 = 1 - 100
                        // Page 2 = 101 - 200
                        // Page 3 = 201 - 300
                        // Page 4 = 301 - 400
                        //out.print(session.getAttribute("test"));

                        for(int i = startIndex; i < startIndex + numOfCoinsOnPage; i++)
                        {
                            JSONObject currentCoinData = (JSONObject)coinDataList.get(i);
                            DecimalFormat withCommas = new DecimalFormat("#,###");
                            DecimalFormat twoDecimalPlaces = new DecimalFormat("#,###.00");

                            out.print("<tr>");
                            out.print("<td>" + (i + 1) + ")</td>");
                            out.print("<td>");
                            out.print("<div class='tooltip'><a href='CoinDataServlet'>" 
                                                            + currentCoinData.get("name") + "</a>");
                            out.print("<span class='tooltiptext'>" + currentCoinData.get("symbol") + "</span>");
                            out.print("</div></td>");
                            out.print("<td>$" + withCommas.format(Double.parseDouble(
                                                                  currentCoinData.get("market_cap_usd")
                                                                                 .toString())) + "</td>");
                            out.print("<td>$" + twoDecimalPlaces.format(Double.parseDouble(
                                                                    currentCoinData.get("price_usd")
                                                                    .toString())) + "</td>");
                            out.print("<td>$" + withCommas.format(Double.parseDouble(
                                                                  currentCoinData.get("24h_volume_usd")
                                                                  .toString())) + "</td>");
                            out.print("<td>" + withCommas.format(Double.parseDouble(
                                                                 currentCoinData.get("total_supply")
                                                                         .toString())) + "</td>");
                            out.print("<td>" + currentCoinData.get("percent_change_24h") + "%</td>");
                            out.print("<td>" + currentCoinData.get("percent_change_7d") + "%</td>");

                            out.print("<td>");

                            // If the coin is a favorite, let the user know
                            if(favoritesList != null && 
                                    favoritesList.contains(currentCoinData.get("symbol")))
                            {
                                out.print("Favorite!");
                            }
                            else    // Otherwise give them the option to make it a favorite
                            {
                                out.print("<form action='CryptoDataServlet' method='POST'>");
                                out.print("<input type='hidden' name='favorite_coin' value='" 
                                        + currentCoinData.get("symbol") + "'/>");
                                out.print("<input type='submit' value='Add to Favorites'/>");
                                out.print("</form>");
                            }

                            out.print("</td>");
                            out.print("</tr>");
                        }
                    %>
                    </tbody>
                </table>
                <div id="pageselect">
                    <form action="CryptoDataServlet" method="GET">
                        <input type="submit" value="Prev" name="prev"/>
                        <input readonly type="number"  value="<% out.print(session.getAttribute("currentPageNum")); %>"
                               min="1" max="<% out.print(session.getAttribute("maxNumOfPages"));%>"/>
                        <input type="submit" value="Next" name="next">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
