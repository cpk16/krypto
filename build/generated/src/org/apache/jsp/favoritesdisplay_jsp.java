package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class favoritesdisplay_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>My Favorite Cryptocurrencies</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"cryptodatadisplay.css\"/>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div id=\"header\">\n");
      out.write("            <h1>Krypto</h1>\n");
      out.write("            ");
 
                String username = "Bilitski";
                boolean userlogin = true;
                if (userlogin)
                {
                    out.print("<h3>" + "Hello, " + username + "! <a href='index.html'>Logout</a></h3>");
                }
            
      out.write("\n");
      out.write("                \n");
      out.write("        </div>\n");
      out.write("        <div id=\"navbar\">\n");
      out.write("\t\t<a href=\"CryptoDataServlet\">Home</a>\n");
      out.write("\t\t<a class=\"active\" href=\"FavoritesServlet\">Favorites</a>\n");
      out.write("\t\t<a href=\"InvestmentsServlet\">Investments</a>\n");
      out.write("\t</div>\n");
      out.write("        \n");
      out.write("        <div id=\"body\">\n");
      out.write("            <h2>My Favorite Cryptocurrencies</h2>\n");
      out.write("            <table id=\"favoritesTable\">\n");
      out.write("                <!-- Table headers -->\n");
      out.write("                <thead>\n");
      out.write("                <tr>\n");
      out.write("                    <th>Name</th>\n");
      out.write("                    <th>Market Cap </th>\n");
      out.write("                    <th>Price </th>\n");
      out.write("                    <th>Trade Volume </th>\n");
      out.write("                    <th>Supply </th>\n");
      out.write("                    <th>24 hour Change </th>\n");
      out.write("                    <th>12 hour Change </th>\n");
      out.write("                    <th>3 day Change </th>\n");
      out.write("                    <th>7 day Change </th>\n");
      out.write("                    <th>1 month Change </th>\n");
      out.write("                    <th></th>\n");
      out.write("                </tr>\n");
      out.write("                </thead>\n");
      out.write("                \n");
      out.write("                <tbody>\n");
      out.write("                <!-- Bitcoin row -->\n");
      out.write("                <tr>\n");
      out.write("                    <td>Bitcoin</td>\n");
      out.write("                    <td>$157,443,256,387</td>\n");
      out.write("                    <td>$9335.69</td>\n");
      out.write("                    <td>$7,267,400,000</td>\n");
      out.write("                    <td>16,864,662</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td><input type=\"button\" value=\"Delete\" onclick=\"deleteRow(this)\"></td>\n");
      out.write("                </tr>\n");
      out.write("                \n");
      out.write("                <!-- Ethereum row -->\n");
      out.write("                <tr>\n");
      out.write("                    <td class=\"name\">Ethereum</td>\n");
      out.write("                    <td>$89,201,209,570</td>\n");
      out.write("                    <td>$913.83</td>\n");
      out.write("                    <td>$2,548,040,000</td>\n");
      out.write("                    <td>97,612,477</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td><input type=\"button\" value=\"Delete\" onclick=\"deleteRow(this)\"></td>\n");
      out.write("                </tr>\n");
      out.write("                \n");
      out.write("                <!-- Ripple row -->\n");
      out.write("                <tr>\n");
      out.write("                    <td class=\"name\">Ripple</td>\n");
      out.write("                    <td>$41,883,804,953</td>\n");
      out.write("                    <td>$1.07</td>\n");
      out.write("                    <td>$900,846,000</td>\n");
      out.write("                    <td>39,009,215,838</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td><input type=\"button\" value=\"Delete\" onclick=\"deleteRow(this)\"></td>\n");
      out.write("                </tr>\n");
      out.write("                \n");
      out.write("                <!-- Bitcoin Cash row -->\n");
      out.write("                <tr>\n");
      out.write("                    <td class=\"name\">Bitcoin Cash</td>\n");
      out.write("                    <td>$23,048,397,300</td>\n");
      out.write("                    <td>$1358.40</td>\n");
      out.write("                    <td>$577,325,000</td>\n");
      out.write("                    <td>16,967,313</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td><input type=\"button\" value=\"Delete\" onclick=\"deleteRow(this)\"></td>\n");
      out.write("                </tr>\n");
      out.write("                \n");
      out.write("                <!-- Litecoin row -->\n");
      out.write("                <tr>\n");
      out.write("                    <td class=\"name\">Litecoin</td>\n");
      out.write("                    <td>$11,599,387,372</td>\n");
      out.write("                    <td>$210.08</td>\n");
      out.write("                    <td>$2,025,430,000</td>\n");
      out.write("                    <td>55,215,458</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td>8.43%</td>\n");
      out.write("                    <td><input type=\"button\" value=\"Delete\" onclick=\"deleteRow(this)\"></td>\n");
      out.write("                </tr>\n");
      out.write("                </tbody>\n");
      out.write("            </table>\n");
      out.write("        </div>\n");
      out.write("            \n");
      out.write("            <script>\n");
      out.write("                function deleteRow(r) {\n");
      out.write("                var i = r.parentNode.parentNode.rowIndex;\n");
      out.write("                document.getElementById(\"favoritesTable\").deleteRow(i);\n");
      out.write("                }\n");
      out.write("            </script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
